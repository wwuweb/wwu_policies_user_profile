<?php
/**
 * @file
 * wwu_policies_user_profile.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wwu_policies_user_profile_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_display_name'.
  $permissions['create field_display_name'] = array(
    'name' => 'create field_display_name',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_display_name'.
  $permissions['edit field_display_name'] = array(
    'name' => 'edit field_display_name',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_display_name'.
  $permissions['edit own field_display_name'] = array(
    'name' => 'edit own field_display_name',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_display_name'.
  $permissions['view field_display_name'] = array(
    'name' => 'view field_display_name',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_display_name'.
  $permissions['view own field_display_name'] = array(
    'name' => 'view own field_display_name',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
